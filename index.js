/*
	DAY 30

  Aggregation and Query Case Studies

*/

db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);

// [SECTION] MongoDB aggregation
/*
	- Used to generate, manipulate, and perform operations to create filtered results that can helps us to analyze the data.
*/

	// Using the aggregate method:
	/*
		- The "$match" is used to pass the document that meet the specified condition conditions(s) to the next stage/aggregation process.
		- Syntax:
			{$match {field: value}}
		- The "$group" is used to group elements together and field-value fairs of the data from the grouped elements.
		- Syntax:
			{$group: {_id: "value", fieldResult: "valueResult"}}
		-Using both $match and $group along with aggregation will find for products are on sale and will group the total amout of stock for all suppliers found
		- The "$" symbol will refer to a field name that is available in the documents that are being aggretated on.
	*/


	db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
		]
	);

	// Field projection with aggregation

	db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id: 0}} // to hide object or element
		]
	);

	// Sorting aggregated results
	/*
		- The "$sort" can be used to change the order of the new aggregated result
		- Syntax:
			{$sort: {field: 1/-1}}
			1 -> lowest to highest
			-1 -> highest to lowest
	*/

	db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}, // sum to ng stock - after ng initial detecttion, dinadagdag nya lang ung sumunod na na detect nya sa "stock" per supplier id
			{$sort: {total: 1}}
		]
	);

	// Aggregating result based on array fields
	/*
		- The "$unwind" deconstruct an array field from a collection/field with an array value to output a result
		- Syntax:
			{$unwind: field}
	*/

	db.fruits.aggregate(
		[
			{$unwind: "$origin"},
			{$group: {_id: "$origin", fruits: {$sum: 1}}} // the code with "sum: 1" - per object na mahahanap nya na may 1, magdadadag dag lang xa ng 1
		]
	)

	// [SECTION] Other aggregate stages

	// count all yellow fruits
	db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}}, // hinanap natin ang color na may "Yellow" sa mga object then
			{$count: "Yellow Fruits"} // bawat count natin pinagalanan natin xa ng "Yellow Fruits" or representing the field "$count"
		]
	)

	// $avg
	// gets the average value of stocl

	db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}},
			{$group: {_id: "$color", yellow_fruit_stock: {$avg: "$stock"}}}
		]
	)

	db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}}, // name changer - pweding baguhin gumagana pa rin
			{$group: {_id: "$color", "Yellow Fruit Stock": {$avg: "$stock"}}}
		]
	)

	// method is a form of function
/*
	let objectName = {
		fieldName1 : value,
		fieldName2 : function(){
			statement
		},
		fieldName3 : value
	}

	objectName.fieldName2(parameter); // para tawagin si fieldName 2
*/
	// $max and $min
	db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}},
			{$group: {_id: "$color", yellow_fruit_stock: {$min: "$stock"}}}
		]
	)

	db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}},
			{$group: {_id: "$color", yellow_fruit_stock: {$max: "$stock"}}}
		]
	)


	// Question from Sir Joemaro
	// Adding a new field to a grouped data
	// $first - returns a field value after grouping
	db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}},
			{$group: {_id: "$color", name : { $first: '$name' }, yellow_fruits_stock: {$min: "$stock"}}}
		]
	)



	// From John Marcelli Habab8:

	db.fruits.aggregate(
	    [
	        {$match:{color: "Yellow"}},
	        {$sort: {stock:1}},
	        {$group: {
	                _id: "$color",
	                name:{"$first" : "$name"}, 
	                yellow_fruits_stock: {$min: "$stock"}
	                }
	            }
	    ]
	)













